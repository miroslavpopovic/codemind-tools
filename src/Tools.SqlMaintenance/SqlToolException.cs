using System;
using System.Runtime.Serialization;

namespace CodeMind.Tools.SqlMaintenance
{
    /// <summary>
    /// Represents an exception that encapsulates any exception happening when running SQL queries using SQL Tool.
    /// </summary>
    [Serializable]
    public class SqlToolException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SqlToolException"/> class.
        /// </summary>
        public SqlToolException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlToolException"/> with serialized data.
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> that holds the data about exception being
        /// thrown.
        /// </param>
        /// <param name="context">The <see cref="StreamingContext"/> that contains contextual information
        /// about the  source or destination.</param>
        /// <exception cref="ArgumentNullException">The <c>info</c> parameter is <c>null</c>.</exception>
        /// <exception cref="SerializationException">The class name is <c>null</c> or 
        /// <see cref="Exception.HResult"/> is zero (0).</exception>
        public SqlToolException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlToolException"/> class with a specified error
        /// message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public SqlToolException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlToolException"/> class with a specified error
        /// message  and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is a cause of the current exception, or a 
        /// <c>null</c> reference if  no inner exception is specified.</param>
        public SqlToolException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
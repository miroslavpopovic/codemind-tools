﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using CodeMind.Tools.SqlMaintenance.Properties;
using log4net;

namespace CodeMind.Tools.SqlMaintenance
{
    /// <summary>
    /// Performs SQL Server maintenance by updating the database schema to a newer version using update scripts.
    /// </summary>
    public class SqlUpdater
    {
        private const string CreateSchemaVersionsTableScript = @"
            IF NOT EXISTS (SELECT * FROM [information_schema].[tables] WHERE table_name = '{0}')
            BEGIN
                CREATE TABLE dbo.{0}
                    (
                    SchemaVersionId int NOT NULL IDENTITY (1, 1),
                    Major varchar(2) NOT NULL,
                    Minor varchar(2) NOT NULL,
                    Revision varchar(4) NOT NULL,
                    ScriptName varchar(50) NOT NULL,
                    AppliedOn datetime NOT NULL
                    )  ON [PRIMARY]
                ALTER TABLE dbo.{0} ADD CONSTRAINT
                    PK_{0} PRIMARY KEY CLUSTERED 
                    (
                    SchemaVersionId
                    ) WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, 
                            ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
            END";

        private static readonly ILog _log = LogManager.GetLogger(typeof(SqlUpdater));

        private const string DefaultChangeScriptPrefix = "db";
        private const string DefaultFileExtension = "sql";
        private const string DefaultPostScriptPrefix = "post";

        private string _scriptsFolder;
        private IList<SchemaVersion> _versions;

        /// <summary>
        /// Creates a new instance of <see cref="SqlUpdater"/> class.
        /// </summary>
        /// <param name="connection"></param>
        public SqlUpdater(Connection connection)
        {
            Connection = connection;
            Repository = new SchemaVersionRepository(Connection);
            QueryRunner = new SqlQueryRunner(Connection);
            SchemaVersionsTableName = "SchemaVersions";

            ReadPropertiesFromConfigFile();
        }

        /// <summary>
        /// Notifies the user on update progress changes.
        /// </summary>
        public event EventHandler<UpdateProgressEventArgs> UpdateProgress;

        /// <summary>
        /// Gets the <see cref="Connection"/> object holding connection string parameters.
        /// </summary>
        public Connection Connection { get; private set; }

        /// <summary>
        /// Gets the latest applied schema version in database.
        /// </summary>
        public SchemaVersion CurrentVersion 
        {
            get { return Versions == null || Versions.Count == 0 ? null : Versions[Versions.Count - 1]; }
        }

        /// <summary>
        /// Gets the list of all applied schema versions in database.
        /// </summary>
        public IList<SchemaVersion> Versions
        {
            get
            {
                if (_versions == null)
                {
                    try
                    {
                        _versions = Repository.GetAll(SchemaVersionsTableName);
                    }
                    catch (SqlException)
                    {
                        // We probably got this exception because there's no SchemaVersions table in database
                        _versions = new List<SchemaVersion>();
                    }
                }
                return _versions;
            }
            private set { _versions = value; }
        }

        private SqlQueryRunner QueryRunner { get; set; }
        private SchemaVersionRepository Repository { get; set; }

        /// <summary>
        /// Gets or sets the name of the folder with update scripts.
        /// </summary>
        public string ScriptsFolder
        {
            get { return _scriptsFolder; }
            set
            {
                if (!Path.IsPathRooted(value))
                     value = Path.Combine(ApplicationPath.GetApplicationPath(), value);
                _scriptsFolder = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the script used to create initial database schema.
        /// </summary>
        public string BaselineName { get; set; }

        /// <summary>
        /// Gets or sets the prefix for post script files.
        /// </summary>
        public string PostFilePrefix { get; set; }

        /// <summary>
        /// Gets or sets the prefix for update script files.
        /// </summary>
        public string FilePrefix { get; set; }

        /// <summary>
        /// Gets or sets the script file extension. Applies to both update and post scripts.
        /// </summary>
        public string FileExtension { get; set; }

        /// <summary>
        /// Gets or sets the name of the SchemaVersions table.
        /// </summary>
        public string SchemaVersionsTableName { get; set; }

        /// <summary>
        /// Gets the update script file name pattern.
        /// </summary>
        private string FilePattern
        {
            get { return String.Format("{0}.*.{1}", FilePrefix, FileExtension); }
        }

        /// <summary>
        /// Gets the post script file name pattern.
        /// </summary>
        private string PostFilePattern
        {
            get { return String.Format("{0}.*.{1}", PostFilePrefix, FileExtension); }
        }

        /// <summary>
        /// Gets the number of applied updates after the execute operation.
        /// </summary>
        public int UpdatesApplied { get; private set; }

        /// <summary>
        /// Runs the update operation.
        /// </summary>
        public void Execute()
        {
            UpdatesApplied = 0;

            OnUpdateProgress(new UpdateProgressEventArgs(Localization.UpdaterStarting));

            if (!Directory.Exists(ScriptsFolder))
            {
                _log.Error(String.Format("Folder not found: {0}", ScriptsFolder));
                throw new SqlUpdaterException(String.Format(Localization.UpdaterFolderNotFound, ScriptsFolder));
            }

            RunCreateSchemaVersionsTableScript();

            if (Versions.Count == 0)
            {
                _log.Debug("No version history found in the database.");
                RunBaselineScript();
            }

            // We have no version informations even after running baseline script
            if (Versions.Count == 0)
            {
                _log.Error("SchemaVersions table is empty. Cannot proceed with updates.");
                throw new SqlUpdaterException(Localization.UpdaterTableEmpty);
            }

            // Runs the update scripts
            RunUpdateScripts();

            // Apply post-scripts if apply-always is specified or if the user chose to apply change scripts
            RunPostScripts();
        }

        /// <summary>
        /// Raises the <see cref="UpdateProgress"/> event.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected virtual void OnUpdateProgress(UpdateProgressEventArgs e)
        {
            if (UpdateProgress != null) 
                UpdateProgress(this, e);
        }

        private string GetChangeFileName(SchemaVersion version)
        {
            return String.Format("{0}.{1}.{2}.{3}.{4}", FilePrefix, version.Major, version.Minor, version.Revision,
                                 FileExtension);
        }

        /// <summary>
        /// Gets the version information from the given file name.
        /// </summary>
        /// <param name="fileName">File name to get the version information for.</param>
        /// <returns>A new instance of <see cref="SchemaVersion"/> class or <c>null</c> if file name is in
        /// wrong format.</returns>
        private SchemaVersion GetVersion(string fileName)
        {
            var version = new SchemaVersion();
            var nameParts = fileName.Split(new[] {'.'});

            if (nameParts.Length != 5 || nameParts[0] != FilePrefix || nameParts[4] != FileExtension) return null;
            version.Major = nameParts[1];
            version.Minor = nameParts[2];
            version.Revision = nameParts[3];
            version.ScriptName = fileName;
            version.AppliedOn = DateTime.Now;

            return version;
        }

        /// <summary>
        /// Read the value for properties from the config file.
        /// </summary>
        private void ReadPropertiesFromConfigFile()
        {
            BaselineName = ConfigurationManager.AppSettings["SqlUpdater.BaselineName"] ?? String.Empty;
            FileExtension = ConfigurationManager.AppSettings["SqlUpdater.FileExtension"];
            FilePrefix = ConfigurationManager.AppSettings["SqlUpdater.FilePrefix"];
            PostFilePrefix = ConfigurationManager.AppSettings["SqlUpdater.PostFilePrefix"];
            SchemaVersionsTableName = 
                ConfigurationManager.AppSettings["SqlUpdater.SchemaVersionsTableName"] ?? "SchemaVersions";
            ScriptsFolder = ConfigurationManager.AppSettings["SqlUpdater.ScriptsFolder"] ?? "Scripts";

            if (String.IsNullOrEmpty(FileExtension))
            {
                FileExtension = DefaultFileExtension;
                _log.DebugFormat("Script file extension is required. Value is not specified. Default value used: {0}",
                                 DefaultFileExtension);
            }
            if (String.IsNullOrEmpty(FilePrefix))
            {
                FilePrefix = DefaultChangeScriptPrefix;
                _log.DebugFormat("SQL change file prefix is required. Value is not specified. Default value used: {0}",
                                 DefaultChangeScriptPrefix);
            }
            if (String.IsNullOrEmpty(PostFilePrefix))
            {
                PostFilePrefix = DefaultPostScriptPrefix;
                _log.DebugFormat("SQL post file prefix is required. Value is not specified. Default value used: {0}",
                                 DefaultChangeScriptPrefix);
            }
            if (FilePrefix.ToLowerInvariant() == PostFilePrefix.ToLowerInvariant())
                throw new Exception(String.Format(Localization.UpdaterPrefixesError, FilePrefix, PostFilePrefix));
        }

        /// <summary>
        /// Invalidates the Versions property cache, forcing it to reload on next access.
        /// </summary>
        private void ReloadVersions()
        {
            Versions = null;
        }

        /// <summary>
        /// Applies the baseline script.
        /// </summary>
        private void RunBaselineScript()
        {
            OnUpdateProgress(new UpdateProgressEventArgs(Localization.UpdaterRunningBaseline));

            var baselineSql = Path.Combine(ScriptsFolder, BaselineName);
            if (!File.Exists(baselineSql))
            {
                _log.Error(String.Format("Baseline file not found: {0}", baselineSql));
                throw new SqlUpdaterException(String.Format(Localization.UpdaterBaselineMissing, baselineSql));
            }
            
            QueryRunner.RunQueryFile(baselineSql);

            ReloadVersions();

            // No versions are found after applying baseline script, so we save the one
            if (Versions.Count != 0) return;

            _log.Warn("Baseline SQL didn't initialize the version history. " +
                      "Initializing the schema to version 01.00.0000.");

            OnUpdateProgress(new UpdateProgressEventArgs(Localization.UpdaterAddingInitialVersion));
            Repository.Save(
                new SchemaVersion
                    {
                        Major = "01",
                        Minor = "00",
                        Revision = "0000",
                        ScriptName = BaselineName,
                        AppliedOn = DateTime.Now
                    }, SchemaVersionsTableName);

            ReloadVersions();
        }

        /// <summary>
        /// Runs the table creation script for SchemaVersions table.
        /// </summary>
        private void RunCreateSchemaVersionsTableScript()
        {
            QueryRunner.RunQuery(String.Format(CreateSchemaVersionsTableScript, SchemaVersionsTableName));
        }

        /// <summary>
        /// Runs all post update script files found in the update folder.
        /// </summary>
        private void RunPostScripts()
        {
            // Get post script files
            var postScripts = GetSqlFiles(ScriptsFolder, PostFilePattern);
            if (postScripts.Count <= 0) return;

            _log.Debug(Localization.UpdaterApplyingPostScripts);
            RunScripts(postScripts, 0, false);
        }

        /// <summary>
        /// Runs all scripts from the given list that have index greater than or equal the given one.
        /// </summary>
        /// <param name="scripts">Scripts to run.</param>
        /// <param name="startIndex">Starting index.</param>
        /// <param name="updateVersionsTable">Should the SchemaVersions table be updated after running each
        /// script.</param>
        private void RunScripts(SortedList<string, string> scripts, int startIndex, bool updateVersionsTable)
        {
            if (scripts.Count == 0) return;

            var keys = scripts.Keys;

            for (var i = startIndex; i < scripts.Count; i++)
            {
                var sqlFile = scripts[keys[i]];
                var scriptName = Path.GetFileName(sqlFile);

                OnUpdateProgress(
                    new UpdateProgressEventArgs(String.Format(Localization.UpdaterApplyingScript, scriptName),
                                                scripts.Count - startIndex, i - startIndex + 1));
                new SqlQueryRunner(Connection).RunQueryFile(sqlFile);

                // Continue if there's no need to update version log (i.e. running post scripts)
                if (!updateVersionsTable) continue;

                // ReSharper disable PossibleNullReferenceException
                var parts = scriptName.Split(new[] { '.' });
                // ReSharper restore PossibleNullReferenceException

                var ver = new SchemaVersion
                              {
                                  ScriptName = scriptName,
                                  Major = parts[1],
                                  Minor = parts[2],
                                  Revision = parts[3],
                                  AppliedOn = DateTime.Now
                              };

                OnUpdateProgress(
                    new UpdateProgressEventArgs(String.Format(Localization.UpdaterSavingVersion, ver.Version),
                                                scripts.Count - startIndex, i - startIndex + 1));

                new SchemaVersionRepository(Connection).Save(ver, SchemaVersionsTableName);

                UpdatesApplied++;
            }
        }

        /// <summary>
        /// Runs necessary update script files found in the update folder.
        /// </summary>
        /// <returns></returns>
        private void RunUpdateScripts()
        {
            var updateScripts = GetSqlFiles(ScriptsFolder, FilePattern);

            if (updateScripts.Count == 0)
            {
                _log.Debug("No updates available");
                return;
            }

            // Find the file that applied the last update
            var lastFileName = GetChangeFileName(CurrentVersion);
            var index = updateScripts.IndexOfKey(lastFileName);

            if (index >= 0) // Exact change file is found, run only it
            {
                if (index + 1 < updateScripts.Count)
                    RunUpdateScripts(updateScripts, index + 1);
                else
                    _log.Debug("No updates available");
            }
            else // If the change file is not found
            {
                for (var i = 0; i < updateScripts.Count; i++)
                {
                    var version = GetVersion(updateScripts.Keys[i]);

                    // This version is already applied?
                    if (version.Compare(CurrentVersion) <= 0) continue;

                    RunUpdateScripts(updateScripts, i);
                    break;
                }
            }
        }

        /// <summary>
        /// Runs all update scripts from the given list that have index greater than or equal the given one.
        /// </summary>
        /// <param name="updateScripts"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        private void RunUpdateScripts(SortedList<string, string> updateScripts, int startIndex)
        {
            _log.Debug("Applying updates ...");
            RunScripts(updateScripts, startIndex, true);
        }

        /// <summary>
        /// Gets all script files from the given folder that match the given pattern.
        /// </summary>
        /// <param name="folder">Folder to search the scripts in.</param>
        /// <param name="pattern">Search pattern.</param>
        /// <returns>A sorted list of script files.</returns>
        private static SortedList<string, string> GetSqlFiles(string folder, string pattern)
        {
            var result = new SortedList<string, string>();

            // Get all script files
            var files = Directory.GetFiles(folder, pattern);
            if (files.Length == 0)
            {
                _log.DebugFormat("No files available here: {0} [{1}]", folder, pattern);
                return result;
            }

            // ReSharper disable AssignNullToNotNullAttribute
            // Sort the files            
            foreach (var file in files)
                result.Add(Path.GetFileName(file), file);
            // ReSharper restore AssignNullToNotNullAttribute

            return result;
        }
    }
}
using System;

namespace CodeMind.Tools.SqlMaintenance
{
    /// <summary>
    /// Represents a schema version entry in version table.
    /// </summary>
    public class SchemaVersion
    {
        private string _major;
        private string _minor;
        private string _revision;
        
        /// <summary>
        /// Gets or sets the date version is applied.
        /// </summary>
        public DateTime AppliedOn { get; set; }

        /// <summary>
        /// Gets or sets the version id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the major version number.
        /// </summary>
        public string Major
        {
            get { return _major.PadLeft(2, '0'); }
            set { _major = value; }
        }

        /// <summary>
        /// Gets or sets the minor version number.
        /// </summary>
        public string Minor
        {
            get { return _minor.PadLeft(2, '0'); }
            set { _minor = value; }
        }
        
        /// <summary>
        /// Gets or sets the revision version number.
        /// </summary>
        public string Revision
        {
            get { return _revision.PadLeft(4, '0'); }
            set { _revision = value; }
        }

        /// <summary>
        /// Gets or sets the name of the applied version script.
        /// </summary>
        public string ScriptName { get; set; }

        /// <summary>
        /// Gets the version number in Major.Minor.Revision format.
        /// </summary>
        public string Version
        {
            get { return String.Format("{0}.{1}.{2}", Major, Minor, Revision); }
        }

        /// <summary>
        /// Compares this version with the given one to find out which one is bigger.
        /// </summary>
        /// <param name="version">Version to compare with.</param>
        /// <returns>A 32-bit signed integer that indicates whether this instance precedes, follows, or
        /// appears in the same position in the sort order as the version parameter.</returns>
        public int Compare(SchemaVersion version)
        {
            return Version.CompareTo(version.Version);
        }
    }
}
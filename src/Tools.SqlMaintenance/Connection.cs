using System;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using CodeMind.Tools.SqlMaintenance.Properties;

namespace CodeMind.Tools.SqlMaintenance
{
    /// <summary>
    /// Holds the connection string informations. It can be provided with the whole connection string, or with parts
    /// which are automatically connected when calling the <see cref="ConnectionString"/> property.
    /// </summary>
    public class Connection
    {
        private string _connectionString = String.Empty;
        private string _database = String.Empty;
        private bool _integratedSecurity;
        private string _password = String.Empty;
        private string _server = String.Empty;
        private string _userName = String.Empty;

        /// <summary>
        /// Creates a new instance of <see cref="Connection"/> class by reading the connection string from the given
        /// configuration connection string name. It first tries to find machine specific connection string
        /// [ConnectionStringName-MachineName], and then the default one [ConnectionStringName]. If neither is found
        /// an empty instance of <see cref="Connection"/> class is returned.
        /// </summary>
        /// <param name="connectionStringName">Name of the configuration connection string.</param>
        /// <returns>A new instance of <see cref="Connection"/> class.</returns>
        public static Connection FromConfigConnectionString(string connectionStringName)
        {
            var connection = new Connection();

            if (!String.IsNullOrEmpty(connectionStringName))
            {
                var machineName = Environment.MachineName;

                var connectionString =
                    ConfigurationManager.ConnectionStrings[connectionStringName + "-" + machineName] ??
                    ConfigurationManager.ConnectionStrings[connectionStringName];

                if (connectionString != null)
                    connection.ConnectionString = connectionString.ConnectionString;
            }

            return connection;
        }

        /// <summary>
        /// Gets or sets the connection string. When connection string is set, all other properties are reset to empty
        /// string. When any other property is set, connection string is cleared and rebuilt from other properties.
        /// </summary>
        public string ConnectionString
        {
            get
            {
                if (String.IsNullOrEmpty(_connectionString))
                    _connectionString = BuildConnectionString(false);
                return _connectionString;
            }
            set
            {
                if (_connectionString == value) return;
                _connectionString = value;
                InitializeProperties();
            }
        }

        /// <summary>
        /// Gets the connection string with master database.
        /// </summary>
        public string MasterConnectionString
        {
            get { return BuildConnectionString(true); }
        }

        /// <summary>
        /// Gets or sets the database name.
        /// </summary>
        public string Database
        {
            get { return _database; }
            set
            {
                if (_database == value) return;
                _database = value;
                ResetConnectionString();
            }
        }

        /// <summary>
        /// Gets or sets the value indicating that the integrated security will be used. When it's set to false, 
        /// <see cref="UserName"/> and <see cref="Password"/> must be defined.
        /// </summary>
        public bool IntegratedSecurity
        {
            get { return _integratedSecurity; }
            set
            {
                if (_integratedSecurity == value) return;
                _integratedSecurity = value;
                ResetConnectionString();
            }
        }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password
        {
            get { return _password; }
            set
            {
                if (_password == value) return;
                _password = value;
                ResetConnectionString();
            }
        }

        /// <summary>
        /// Gets or sets the server name.
        /// </summary>
        public string Server
        {
            get { return _server; }
            set
            {
                if (_server == value) return;
                _server = value;
                ResetConnectionString();
            }
        }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName
        {
            get { return _userName; }
            set
            {
                if (_userName == value) return;
                _userName = value;
                ResetConnectionString();
            }
        }

        /// <summary>
        /// Checks to see if the current connection is on a local server.
        /// </summary>
        /// <returns><c>true</c> if the connection is on local server, <c>false</c> otherwise.</returns>
        public bool IsOnLocalServer()
        {
            return Regex.Match(Server, @"^(?:\.|\(local\)|localhost|127.0.0.1).*$").Success;
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public override string ToString()
        {
            var result = new StringBuilder();

            result.AppendFormat("{0}: {1}\n", Localization.ConnectionServer, Server);
            result.AppendFormat("{0}: {1}\n", Localization.ConnectionDatabase, Database);
            if (IntegratedSecurity)
                result.AppendLine(Localization.ConnectionIntegratedSecurity);
            else
                result.AppendFormat("{0}: {1}\n", Localization.ConnectionUser, UserName);

            return result.ToString();
        }

        private string BuildConnectionString(bool useMasterDb)
        {
            var result = new StringBuilder(50);
            result.AppendFormat("Data Source={0};Initial Catalog={1};", Server, useMasterDb ? "master" : Database);

            if (IntegratedSecurity)
                result.Append("Integrated Security=True;");
            else
                result.AppendFormat("User ID={0};Password={1};", UserName, Password);

            return result.ToString();
        }

        private void InitializeProperties()
        {
            _database =
                Regex.Match(_connectionString, @"(?:Database=|Initial Catalog=)([a-zA-Z0-9_\-\.]*)",
                            RegexOptions.IgnoreCase).Groups[1].Value;
            _integratedSecurity =
                Regex.Match(_connectionString, @"(?:Integrated Security=|Trusted_Connection=)([sspi|yes|true]*)",
                            RegexOptions.IgnoreCase).Groups[1].Success;
            _password = _integratedSecurity
                            ? String.Empty
                            : Regex.Match(_connectionString, @"(?:Password=|Pwd=)([a-zA-Z0-9\-\.]*)",
                                          RegexOptions.IgnoreCase).Groups[1].Value;
            _server =
                Regex.Match(_connectionString, @"(?:Server=|Data Source=)([a-zA-Z0-9_\-\.\\\(\),]*)",
                            RegexOptions.IgnoreCase).Groups[1].Value;
            _userName = _integratedSecurity
                            ? String.Empty
                            : Regex.Match(_connectionString, @"(?:User ID=|Uid=)([a-zA-Z0-9\-\.]*)",
                                          RegexOptions.IgnoreCase).Groups[1].Value;
        }

        private void ResetConnectionString()
        {
            _connectionString = String.Empty;
        }
    }
}
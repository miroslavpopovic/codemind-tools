using System;
using System.IO;
using System.Reflection;

namespace CodeMind.Tools.SqlMaintenance
{
    internal static class ApplicationPath
    {
        public static string GetApplicationPath()
        {
            var location = Assembly.GetCallingAssembly().Location;
            var uri = new Uri(location);
            return Path.GetDirectoryName(uri.LocalPath);
        }
    }
}
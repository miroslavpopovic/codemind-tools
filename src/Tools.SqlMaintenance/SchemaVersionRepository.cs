using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using log4net;

namespace CodeMind.Tools.SqlMaintenance
{
    /// <summary>
    /// Repository for <see cref="SchemaVersion"/> objects.
    /// </summary>
    internal class SchemaVersionRepository
    {
        private const string GetAllQuery = @"
            SELECT SchemaVersionId, Major, Minor, Revision, ScriptName, AppliedOn
            FROM {0}
            ORDER BY Major, Minor, Revision, SchemaVersionId";
        private const string InsertQuery = @"
            INSERT INTO {0} (Major, Minor, Revision, ScriptName, AppliedOn) 
            VALUES (@Major, @Minor, @Revision, @ScriptName, @AppliedOn);
            SELECT SCOPE_IDENTITY()";

        private static readonly ILog _log = LogManager.GetLogger(typeof (SqlUpdater));

        /// <summary>
        /// Creates a new instance of <see cref="SchemaVersionRepository"/> class.
        /// </summary>
        /// <param name="connection">Connection parameters.</param>
        public SchemaVersionRepository(Connection connection)
        {
            Connection = connection;
        }

        /// <summary>
        /// Gets the <see cref="Connection"/> object holding connection string.
        /// </summary>
        public Connection Connection { get; private set; }

        /// <summary>
        /// Gets the list of all persisted <see cref="SchemaVersion"/> objects.
        /// </summary>
        /// <param name="tableName">Name of the Schema Versions table.</param>
        /// <returns>A list of <see cref="SchemaVersion"/> objects.</returns>
        public IList<SchemaVersion> GetAll(string tableName)
        {
            var schemaVersions = new List<SchemaVersion>();

            using (var conn = new SqlConnection(Connection.ConnectionString))
            using (var cmd = conn.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = String.Format(GetAllQuery, tableName);

                try
                {
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var schemaVersion = 
                                new SchemaVersion
                                    {
                                        Id = reader.GetInt32(reader.GetOrdinal("SchemaVersionId")),
                                        Major = reader.GetString(reader.GetOrdinal("Major")),
                                        Minor = reader.GetString(reader.GetOrdinal("Minor")),
                                        Revision = reader.GetString(reader.GetOrdinal("Revision")),
                                        ScriptName = reader.GetString(reader.GetOrdinal("ScriptName")),
                                        AppliedOn = reader.GetDateTime(reader.GetOrdinal("AppliedOn"))
                                    };
                            schemaVersions.Add(schemaVersion);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    _log.Error("Error loading Schema Version information", ex);
                    throw;
                }
            }

            return schemaVersions;
        }

        /// <summary>
        /// Saves the given <see cref="SchemaVersion"/> object.
        /// </summary>
        /// <param name="version"><see cref="SchemaVersion"/> to save.</param>
        /// <param name="tableName">Name of the Schema Versions table.</param>
        public void Save(SchemaVersion version, string tableName)
        {
            using (var conn = new SqlConnection(Connection.ConnectionString))
            using (var cmd = conn.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = String.Format(InsertQuery, tableName);

                cmd.Parameters.AddWithValue("@Major", version.Major);
                cmd.Parameters.AddWithValue("@Minor", version.Minor);
                cmd.Parameters.AddWithValue("@Revision", version.Revision);
                cmd.Parameters.AddWithValue("@ScriptName", version.ScriptName);
                cmd.Parameters.AddWithValue("@AppliedOn", version.AppliedOn);

                try
                {
                    conn.Open();

                    version.Id = Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (SqlException ex)
                {
                    _log.Error("Error saving Schema Version information", ex);
                    throw;
                }
            }
        }
    }
}
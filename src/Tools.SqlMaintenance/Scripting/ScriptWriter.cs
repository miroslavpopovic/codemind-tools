using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;

namespace CodeMind.Tools.SqlMaintenance.Scripting
{
    /// <summary>
    /// Generates INSERT scripts for one or more tables.
    /// </summary>
    public class ScriptWriter
    {
        private class ColumnDefinition
        {
            public string Name { get; set; }
            public Type Type { get; set; }
        }

        private readonly IList<TableDefinition> _tables;
        private readonly string _connectionString;

        /// <summary>
        /// Creates a new instance of <see cref="ScriptWriter" /> class.
        /// </summary>
        /// <param name="tables">List of table names to script. Table names are separated with comma ",".</param>
        /// <param name="connectionString">Database connection string.</param>
        public ScriptWriter(IList<TableDefinition> tables, string connectionString)
        {
            _tables = tables;
            _connectionString = connectionString;
        }

        /// <summary>
        /// Writes the INSERT scripts and returns them as a string to the caller.
        /// </summary>
        /// <returns></returns>
        public string WriteScript()
        {
            var scriptBuilder = new StringBuilder();
            using (var writer = new StringWriter(scriptBuilder))
            {
                WriteScript(writer);
            }
            return scriptBuilder.ToString();
        }

        /// <summary>
        /// Writes the INSERT scripts to the given writer.
        /// </summary>
        /// <param name="writer"><see cref="TextWriter"/> used to output INSERT scripts.</param>
        public void WriteScript(TextWriter writer)
        {
            using (var conn = new SqlConnection(_connectionString))
            {
                var command = conn.CreateCommand();
                conn.Open();

                writer.WriteLine("/***** Script Date: {0} *****/", DateTime.Now);
                writer.WriteLine();

                foreach (var table in _tables)
                {
                    ProcessTable(table, command, writer);
                }
            }
        }

        private static void ProcessTable(TableDefinition table, SqlCommand command, TextWriter writer)
        {
            command.CommandText = String.Format("SELECT * FROM {0}", table.EscapedName);

            using (var reader = command.ExecuteReader())
            {
                // This is where the "INSERT INTO [table] (....) VALUES (" will be stored
                var insertCommandHeaderBuilder = new StringBuilder();
                insertCommandHeaderBuilder.AppendFormat("INSERT INTO {0} (", table.EscapedName);

                var columns = new List<ColumnDefinition>(reader.FieldCount);
                for (var i = 0; i < reader.FieldCount; i++)
                {
                    columns.Add(new ColumnDefinition { Name = reader.GetName(i), Type = reader.GetFieldType(i) });

                    if (i > 0) insertCommandHeaderBuilder.Append(", ");
                    insertCommandHeaderBuilder.AppendFormat("[{0}]", reader.GetName(i));
                }

                if (columns.Count == 0) return;

                insertCommandHeaderBuilder.Append(") VALUES (");

                writer.WriteLine("/***** Object: Table {0} *****/", table.EscapedName);

                if (table.IsIdentity) writer.WriteLine("SET IDENTITY_INSERT {0} ON", table.EscapedName);

                SerializeRow(table, columns, reader, writer, insertCommandHeaderBuilder.ToString());

                if (table.IsIdentity) writer.WriteLine("SET IDENTITY_INSERT {0} OFF", table.EscapedName);

                writer.WriteLine("GO");
                writer.WriteLine();
            }
        }

        private static void SerializeRow(TableDefinition table, IList<ColumnDefinition> columns,
            IDataReader reader, TextWriter writer, string insertCommandHeader)
        {
            var idColumns = new List<ColumnDefinition>();
            var idColumnNames =
                (String.IsNullOrEmpty(table.IdColumnNames) ? columns[0].Name : table.IdColumnNames).Split(',');

            foreach (var column in columns)
                foreach (var name in idColumnNames)
                    if (column.Name == name) idColumns.Add(column);

            while (reader.Read())
            {
                
                if (table.CheckIfExists)
                    writer.Write("IF NOT EXISTS (SELECT * FROM {0} {1})\n    ",
                                 table.EscapedName, SerializeToWhereClause(idColumns, reader));

                writer.Write(insertCommandHeader);
                for (var i = 0; i < columns.Count; i++)
                {
                    if (i > 0) writer.Write(", ");
                    writer.Write(SerializeFieldValue(reader[i], columns[i].Type));
                }
                writer.WriteLine(")");

                if (table.CheckIfExists && table.UpdateIfExists)
                {
                    writer.Write("ELSE\n    UPDATE {0} SET ", table.EscapedName);

                    for (var i = 0; i < columns.Count; i++)
                    {
                        if (idColumns.Contains(columns[i])) continue;
                        writer.Write("[{0}] = {1}", columns[i].Name, SerializeFieldValue(reader[i], columns[i].Type));
                        if (i < columns.Count - 1) writer.Write(", ");
                    }

                    writer.WriteLine(SerializeToWhereClause(idColumns, reader));
                }
            }
        }

        private static string SerializeFieldValue(object value, Type columnType)
        {
            if (value == null || value == DBNull.Value)
                return "NULL";
            if (columnType == typeof(string) || columnType == typeof(Guid))
            {
                // Avoid problems with single quotation in insert
                var serializedValue = value.ToString().Replace("'", "''");
                return String.Format("N'{0}'", serializedValue);
            }
            if (columnType == typeof(DateTime))
                return String.Format("'{0:yyyyMMdd HH:mm:ss}'", value);
            if (columnType == typeof(byte[]))
                return "0x" + BitConverter.ToString((byte[])value).Replace("-", String.Empty);
            if (columnType == typeof(bool))
                return (bool)value ? "1" : "0";

            return String.Format(CultureInfo.InvariantCulture, "{0}", value);
        }

        private static string SerializeToWhereClause(IEnumerable<ColumnDefinition> columns, IDataReader reader)
        {
            var result = new StringBuilder("WHERE ");
            var first = true;

            foreach (var column in columns)
            {
                if (!first) result.Append(" AND ");
                result.AppendFormat("[{0}] = {1}", column.Name, SerializeFieldValue(reader[column.Name], column.Type));
                first = false;
            }

            return result.ToString();
        }
    }
}
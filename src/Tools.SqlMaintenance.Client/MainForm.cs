﻿using System;
using System.Configuration;
using System.IO;
using System.Windows.Forms;
using CodeMind.Tools.SqlMaintenance.Client.Properties;

namespace CodeMind.Tools.SqlMaintenance.Client
{
    internal partial class MainForm : Form
    {
        private Connection _connection;

        public MainForm()
        {
            InitializeComponent();
        }

        public Connection Connection
        {
            get { return _connection; }
            set
            {
                _connection = value;
                DisplayConnection();
                UpdateInterface();
            }
        }

        private void DisplayConnection()
        {
            connectionLabel.Text = Connection == null ? Localization.MainFormNoConnection : Connection.ToString();
        }

        private void UpdateInterface()
        {
            createBackupButton.Enabled = Connection != null;
            restoreBackupButton.Enabled = Connection != null;
            upgradeButton.Enabled = Connection != null;
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void createBackupButton_Click(object sender, EventArgs e)
        {
            var fileName = GetBackupFileName();

            var saveFileDialog =
                new SaveFileDialog
                    {
                        DefaultExt = ".bak",
                        FileName = fileName,
                        Filter = Localization.MainFormSqlBackupFiles + " (*.bak;*.zip)|*.bak;*.zip|" +
                                 Localization.MainFormAllFiles + " (*.*)|*.*",
                        Title = Localization.MainFormSaveBackupTitle,
                        InitialDirectory = GetBackupPath()
                    };

            if (saveFileDialog.ShowDialog(this) != DialogResult.OK) return;
            var backup = new DatabaseBackup {Connection = Connection, BackupFilePath = saveFileDialog.FileName};

            Cursor = Cursors.WaitCursor;
            try
            {
                backup.PerformBackup();
                backup.DeleteOldBackups(GetBackupPath());
                MessageBox.Show(Localization.MainFormBackupSuccessful, Localization.MainFormBackupTitle,
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (ServerScopeException)
            {
                MessageBox.Show(Localization.BackupLocalServerOnly, Localization.Error, MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0}\n\n{1}", Localization.MainFormBackupError, ex.Message),
                                Localization.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private static string GetBackupPath()
        {
            var backupPath = ConfigurationManager.AppSettings["Backup.DefaultLocation"];
            if (!Directory.Exists(backupPath))
                Directory.CreateDirectory(backupPath);
            return backupPath;
        }

        private string GetBackupFileName()
        {
            var fileNameTemplate = ConfigurationManager.AppSettings["Backup.FileNameTemplate"];
            return fileNameTemplate
                       .Replace("%DatabaseName%", Connection.Database)
                       .Replace("%Timestamp%", DateTime.Now.ToString("yyyy-MM-dd-HH-mm")) + ".bak";
        }

        private void restoreBackupButton_Click(object sender, EventArgs e)
        {
            var openFileDialog =
                new OpenFileDialog
                    {
                        Filter = Localization.MainFormSqlBackupFiles + " (*.bak;*.zip)|*.bak;*.zip|" +
                                 Localization.MainFormAllFiles + " (*.*)|*.*",
                        DefaultExt = ".bak",
                        Title = Localization.MainFormOpenBackupTitle
                    };

            if (openFileDialog.ShowDialog(this) != DialogResult.OK) return;

            var backup = new DatabaseBackup {Connection = Connection, BackupFilePath = openFileDialog.FileName};

            Cursor = Cursors.WaitCursor;
            try
            {
                backup.PerformRestore();
                MessageBox.Show(Localization.MainFormRestoreSuccessful, Localization.MainFormRestoreTitle,
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (ServerScopeException)
            {
                MessageBox.Show(Localization.RestoreLocalServerOnly, Localization.Error, MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0}\n\n{1}", Localization.MainFormRestoreError, ex.Message),
                                Localization.Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void upgradeButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, Localization.MainFormApplyUpdates, Localization.MainFormApplyUpdatesTitle,
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) !=
                DialogResult.Yes)
                return;

            var backupFilePath = Path.Combine(GetBackupPath(), GetBackupFileName());
            var updateProgressDialog = new UpdateProgressDialog(Connection, backupFilePath);
            updateProgressDialog.ShowDialog(this);
        }
    }
}
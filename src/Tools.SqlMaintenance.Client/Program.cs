﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using CodeMind.Tools.SqlMaintenance.Client.Properties;
using log4net.Config;

namespace CodeMind.Tools.SqlMaintenance.Client
{
    internal static class Program
    {
        private static Connection Connection { get; set; }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            XmlConfigurator.Configure();

            var language = ConfigurationManager.AppSettings["Language"] ?? String.Empty;
            if (language != String.Empty)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(language);
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(language);
            }

            var arguments = new Arguments(args);
            Connection = InitializeConnection(arguments);
            InitializeConfigDefaults();
            OverrideConfigFromCommandLine(arguments);

            if (arguments.Exists("?"))
            {
                ShowHelp();
                return;
            }

            if (arguments.Exists("b") || arguments.Exists("backup") || arguments.Exists("r") ||
                arguments.Exists("restore") || arguments.Exists("update"))
            {
                RunConsoleInterface(arguments);
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm {Connection = Connection});
        }

        private static void CreateBackup(string destination)
        {
            // Prepare output file name
            if (String.IsNullOrEmpty(destination))
                destination = ConfigurationManager.AppSettings["Backup.DefaultLocation"];
            if (Path.GetExtension(destination) == String.Empty)
            {
                var fileName = ConfigurationManager.AppSettings["Backup.FileNameTemplate"]
                                   .Replace("%DatabaseName%", Connection.Database)
                                   .Replace("%Timestamp%", DateTime.Now.ToString("yyyy-MM-dd-HH-mm")) + ".bak";
                destination += fileName;
            }
            if (!Path.IsPathRooted(destination))
                destination = Path.Combine(ConfigurationManager.AppSettings["Backup.DefaultLocation"], destination);

            // ReSharper disable AssignNullToNotNullAttribute
            if (!Directory.Exists(Path.GetDirectoryName(destination)))
                Directory.CreateDirectory(Path.GetDirectoryName(destination));
            // ReSharper restore AssignNullToNotNullAttribute

            var backup = new DatabaseBackup {Connection = Connection, BackupFilePath = destination};

            try
            {
                Console.WriteLine(Localization.ProgramCreateBackup, Connection.Database);
                backup.PerformBackup();
                backup.DeleteOldBackups(Path.GetDirectoryName(destination));
                Console.WriteLine(Localization.ProgramBackupCreated, destination);
            }
            catch (ServerScopeException)
            {
                Console.WriteLine("{0}: {1}", Localization.Error, Localization.BackupLocalServerOnly);
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0}: {1}", Localization.Error, ex);
            }
        }

        private static string GetConnectionStringName(Arguments arguments)
        {
            var connectionStringName = String.Empty;
            
            if (arguments.Exists("c"))
                connectionStringName = arguments.Single("c");
            if (arguments.Exists("connection"))
                connectionStringName = arguments.Single("connection");
            if (connectionStringName == String.Empty)
                connectionStringName = ConfigurationManager.AppSettings["connectionStringName"];
            if (String.IsNullOrEmpty(connectionStringName))
                connectionStringName = "database";
            return connectionStringName;
        }

        private static string GetHelpLine(string argument, string description)
        {
            return argument.PadRight(30) + description;
        }

        private static void InitializeConfigDefaults()
        {
            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["Backup.FileNameTemplate"]))
                ConfigurationManager.AppSettings["Backup.FileNameTemplate"] = "%DatabaseName%-%Timestamp%";

            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["Backup.DefaultLocation"]))
                ConfigurationManager.AppSettings["Backup.DefaultLocation"] = "%MyDocuments%\\My DB Backups";
            ConfigurationManager.AppSettings["Backup.DefaultLocation"] =
                ConfigurationManager.AppSettings["Backup.DefaultLocation"]
                    .Replace("%MyDocuments%", Environment.GetFolderPath(Environment.SpecialFolder.Personal));
        }

        private static Connection InitializeConnection(Arguments arguments)
        {
            var connectionStringName = GetConnectionStringName(arguments);
            var connection = Connection.FromConfigConnectionString(connectionStringName);

            // And then we apply any overrides defined through command line parameters
            if (arguments.Exists("s"))
                connection.Server = arguments.Single("s");
            if (arguments.Exists("server"))
                connection.Server = arguments.Single("server");
            if (arguments.Exists("d"))
                connection.Database = arguments.Single("d");
            if (arguments.Exists("db"))
                connection.Database = arguments.Single("db");
            if (arguments.Exists("u"))
                connection.UserName = arguments.Single("u");
            if (arguments.Exists("user"))
                connection.UserName = arguments.Single("user");
            if (arguments.Exists("p"))
                connection.Password = arguments.Single("p");
            if (arguments.Exists("pass"))
                connection.Password = arguments.Single("pass");
            if (arguments.Exists("u") || arguments.Exists("user") || arguments.Exists("p") || arguments.Exists("pass"))
                connection.IntegratedSecurity = false;

            return connection;
        }

        private static void OverrideConfigFromCommandLine(Arguments arguments)
        {
            if (arguments.Exists("scriptFolder"))
                ConfigurationManager.AppSettings["SqlUpdater.ScriptsFolder"] = arguments.Single("scriptFolder");
            if (arguments.Exists("ext"))
                ConfigurationManager.AppSettings["SqlUpdater.FileExtension"] = arguments.Single("ext");
            if (arguments.Exists("prefix"))
                ConfigurationManager.AppSettings["SqlUpdater.FilePrefix"] = arguments.Single("prefix");
            if (arguments.Exists("postPrefix"))
                ConfigurationManager.AppSettings["SqlUpdater.PostFilePrefix"] = arguments.Single("postPrefix");
            if (arguments.Exists("baseline"))
                ConfigurationManager.AppSettings["SqlUpdater.BaselineName"] = arguments.Single("baseline");
            if (arguments.Exists("versionsTable"))
                ConfigurationManager.AppSettings["SqlUpdater.SchemaVersionsTableName"] = 
                    arguments.Single("versionsTable");
            if (arguments.Exists("backupBeforeUpdate"))
                ConfigurationManager.AppSettings["SqlUpdater.BackupBeforeUpdate"] = "true";
            else if (arguments.Exists("noBackupBeforeUpdate"))
                ConfigurationManager.AppSettings["SqlUpdater.BackupBeforeUpdate"] = "false";
        }

        private static void ShowHelp()
        {
            var help = new StringBuilder();

            help.AppendLine(GetHelpLine(Localization.ArgumentTitle, Localization.ArgumentDescription));
            help.AppendLine(new string('-', 80));
            help.AppendLine(GetHelpLine("-c(onnection) <connection string name>", Localization.ArgumentConnection));
            help.AppendLine(GetHelpLine("-s(erver) <server name>", Localization.ArgumentServerName));
            help.AppendLine(GetHelpLine("-d(b) <db name>", Localization.ArgumentDbName));
            help.AppendLine(GetHelpLine("-u(ser) <user name>", Localization.ArgumentUserName));
            help.AppendLine(GetHelpLine("-p(ass) <password>", Localization.ArgumentPassword));
            help.AppendLine(GetHelpLine("-b(ackup) <destination>", Localization.ArgumentBackup));
            help.AppendLine(GetHelpLine("-r(estore) <backup file>", Localization.ArgumentRestore));
            help.AppendLine(GetHelpLine("-update", Localization.ArgumentUpdate));
            help.AppendLine(GetHelpLine("-backupBeforeUpdate", Localization.ArgumentBackupBeforeUpdate));
            help.AppendLine(GetHelpLine("-noBackupBeforeUpdate", Localization.ArgumentNoBackupBeforeUpdate));
            help.AppendLine(GetHelpLine("-scriptFolder <folder name>", Localization.ArgumentScriptFolder));
            help.AppendLine(GetHelpLine("-ext <extension>", Localization.ArgumentExtension));
            help.AppendLine(GetHelpLine("-prefix <prefix>", Localization.ArgumentPrefix));
            help.AppendLine(GetHelpLine("-postPrefix <prefix>", Localization.ArgumentPostPrefix));
            help.AppendLine(GetHelpLine("-baseline <file name>", Localization.ArgumentBaseline));
            help.AppendLine(GetHelpLine("-versionsTable <name>", Localization.ArgumentVersionsTable));
            help.AppendLine(GetHelpLine("-?", Localization.ArgumentHelp));
            help.AppendLine();

            Console.Write(help.ToString());
        }

        private static void RestoreBackup(string backupFileName)
        {
            if (!Path.IsPathRooted(backupFileName))
                backupFileName = Path.Combine(ConfigurationManager.AppSettings["Backup.DefaultLocation"], backupFileName);

            var backup = new DatabaseBackup {Connection = Connection, BackupFilePath = backupFileName};

            try
            {
                Console.WriteLine("Restoring database \"{0}\"...", Connection.Database);
                backup.PerformRestore();
                Console.WriteLine("Restore successful");
            }
            catch (ServerScopeException)
            {
                Console.WriteLine("{0}: {1}", Localization.Error, Localization.RestoreLocalServerOnly);
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0}: {1}", Localization.Error, ex);
            }
        }

        private static void RunConsoleInterface(Arguments arguments)
        {
            if (arguments.Exists("b"))
            {
                CreateBackup(arguments.Single("b"));
                return;
            }
            if (arguments.Exists("backup"))
            {
                CreateBackup(arguments.Single("backup"));
                return;
            }

            if (arguments.Exists("r"))
            {
                RestoreBackup(arguments.Single("r"));
                return;
            }
            if (arguments.Exists("restore"))
            {
                RestoreBackup(arguments.Single("restore"));
                return;
            }

            if (!arguments.Exists("update")) return;

            UpdateDatabase();
        }

        private static void UpdateDatabase()
        {
            bool backupBeforeUpdate;
            Boolean.TryParse(ConfigurationManager.AppSettings["SqlUpdater.BackupBeforeUpdate"], out backupBeforeUpdate);

            if (backupBeforeUpdate) CreateBackup(String.Empty);

            var updater = new SqlUpdater(Connection);
            updater.UpdateProgress += UpdateProgres;

            try
            {
                updater.Execute();

                Console.WriteLine();
                if (updater.UpdatesApplied == 0)
                    Console.WriteLine(Localization.ProgramNoUpdates);
                else
                    Console.WriteLine(Localization.ProgramUpdatesApplied, updater.UpdatesApplied);
            }
            catch (Exception ex)
            {
                Console.WriteLine(Localization.ProgramUpdateError, ex);
                Console.WriteLine();
            }
        }

        private static void UpdateProgres(object sender, UpdateProgressEventArgs e)
        {
            if (e.TotalActionCount == 0)
                Console.WriteLine(e.Message);
            else
                Console.WriteLine(Localization.ProgramApplyingUpdate, e.CurrentActionNumber, e.TotalActionCount,
                                  e.Message);
        }
    }
}
﻿namespace CodeMind.Tools.SqlMaintenance.Client
{
    internal partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.connectionLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.createBackupButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.upgradeButton = new System.Windows.Forms.Button();
            this.restoreBackupButton = new System.Windows.Forms.Button();
            this.changeDatabaseButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // connectionLabel
            // 
            resources.ApplyResources(this.connectionLabel, "connectionLabel");
            this.connectionLabel.Name = "connectionLabel";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.RoyalBlue;
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Name = "label1";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.RoyalBlue;
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Name = "label3";
            // 
            // createBackupButton
            // 
            resources.ApplyResources(this.createBackupButton, "createBackupButton");
            this.createBackupButton.Image = global::CodeMind.Tools.SqlMaintenance.Client.Properties.Resources.Backup;
            this.createBackupButton.Name = "createBackupButton";
            this.createBackupButton.UseVisualStyleBackColor = true;
            this.createBackupButton.Click += new System.EventHandler(this.createBackupButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.exitButton, "exitButton");
            this.exitButton.Image = global::CodeMind.Tools.SqlMaintenance.Client.Properties.Resources.Exit;
            this.exitButton.Name = "exitButton";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CodeMind.Tools.SqlMaintenance.Client.Properties.Resources.Database;
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // upgradeButton
            // 
            resources.ApplyResources(this.upgradeButton, "upgradeButton");
            this.upgradeButton.Image = global::CodeMind.Tools.SqlMaintenance.Client.Properties.Resources.Upgrade;
            this.upgradeButton.Name = "upgradeButton";
            this.upgradeButton.UseVisualStyleBackColor = true;
            this.upgradeButton.Click += new System.EventHandler(this.upgradeButton_Click);
            // 
            // restoreBackupButton
            // 
            resources.ApplyResources(this.restoreBackupButton, "restoreBackupButton");
            this.restoreBackupButton.Image = global::CodeMind.Tools.SqlMaintenance.Client.Properties.Resources.Restore;
            this.restoreBackupButton.Name = "restoreBackupButton";
            this.restoreBackupButton.UseVisualStyleBackColor = true;
            this.restoreBackupButton.Click += new System.EventHandler(this.restoreBackupButton_Click);
            // 
            // changeDatabaseButton
            // 
            resources.ApplyResources(this.changeDatabaseButton, "changeDatabaseButton");
            this.changeDatabaseButton.Image = global::CodeMind.Tools.SqlMaintenance.Client.Properties.Resources.ChangeDatabase;
            this.changeDatabaseButton.Name = "changeDatabaseButton";
            this.changeDatabaseButton.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AcceptButton = this.createBackupButton;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.exitButton;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.upgradeButton);
            this.Controls.Add(this.restoreBackupButton);
            this.Controls.Add(this.createBackupButton);
            this.Controls.Add(this.changeDatabaseButton);
            this.Controls.Add(this.connectionLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label connectionLabel;
        private System.Windows.Forms.Button changeDatabaseButton;
        private System.Windows.Forms.Button createBackupButton;
        private System.Windows.Forms.Button restoreBackupButton;
        private System.Windows.Forms.Button upgradeButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button exitButton;
    }
}
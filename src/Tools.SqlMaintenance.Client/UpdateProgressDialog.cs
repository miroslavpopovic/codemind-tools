﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Windows.Forms;
using CodeMind.Tools.SqlMaintenance.Client.Properties;
using log4net;

namespace CodeMind.Tools.SqlMaintenance.Client
{
    internal partial class UpdateProgressDialog : Form
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (UpdateProgressDialog));

        public UpdateProgressDialog(Connection connection, string backupFilePath)
        {
            Connection = connection;
            BackupFilePath = backupFilePath;
            InitializeComponent();
        }

        public Connection Connection { get; private set; }
        public string BackupFilePath { get; private set; }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            statusLabel.Text = Localization.UpdateCreateBackup;

            bool backupBeforeUpdate;
            Boolean.TryParse(ConfigurationManager.AppSettings["SqlUpdater.BackupBeforeUpdate"], out backupBeforeUpdate);

            if (backupBeforeUpdate)
            {
                var backup = new DatabaseBackup {Connection = Connection, BackupFilePath = BackupFilePath};
                try
                {
                    backup.PerformBackup();
                    statusLabel.Text = Localization.UpdateBackupSuccessful;
                }
                catch (Exception ex)
                {
                    _log.Error("Creating backup before update", ex);

                    if (MessageBox.Show(Localization.UpdateBackupError, Localization.Error,
                                        MessageBoxButtons.YesNoCancel, MessageBoxIcon.Error) != DialogResult.Yes)
                    {
                        Close();
                        return;
                    }
                }
            }

            backgroundWorker.RunWorkerAsync();
        }

        private void OnBackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            var updater = new SqlUpdater(Connection);
            updater.UpdateProgress += OnSqlUpdaterUpdateProgress;
            updater.Execute();

            e.Result = updater.UpdatesApplied;
        }

        private void OnSqlUpdaterUpdateProgress(object sender, UpdateProgressEventArgs e)
        {
            backgroundWorker.ReportProgress(0, e);
        }

        private void OnBackgroundWorkerProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var progressArgs = (UpdateProgressEventArgs) e.UserState;

            if (progressArgs.TotalActionCount == 0)
                progressBar.Style = ProgressBarStyle.Marquee;
            else
            {
                progressBar.Style = ProgressBarStyle.Blocks;
                progressBar.Maximum = progressArgs.TotalActionCount;
                progressBar.Value = progressArgs.CurrentActionNumber;
                progressBar.Invalidate();
            }

            statusLabel.Text = progressArgs.Message;
        }

        private void OnBackgroundWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
                MessageBox.Show(string.Format("{0}\n\n{1}", Localization.UpdateError, e.Error.Message),
                                Localization.UpdateTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                if ((int) e.Result == 0)
                    MessageBox.Show(Localization.UpdateNoUpdates, Localization.UpdateTitle, MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                else
                    MessageBox.Show(String.Format(Localization.UpdateApplied, e.Result), Localization.UpdateCompleted,
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            Close();
        }
    }
}
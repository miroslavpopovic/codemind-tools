/*
   2. septembar 200916:38:33
   User: 
   Server: QUADCORE\SQLEXPRESS
   Database: TestDatabase
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Orders
	(
	OrderId int NOT NULL IDENTITY (1, 1),
	Date datetime NOT NULL,
	CustomerId int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Orders ADD CONSTRAINT
	PK_Orders PRIMARY KEY CLUSTERED 
	(
	OrderId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Orders ADD CONSTRAINT
	FK_Orders_Customers FOREIGN KEY
	(
	CustomerId
	) REFERENCES dbo.Customers
	(
	CustomerId
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
COMMIT

/*
   2. septembar 200916:41:03
   User: 
   Server: QUADCORE\SQLEXPRESS
   Database: TestDatabase
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.OrderDetails
	(
	OrderDetailId int NOT NULL IDENTITY (1, 1),
	OrderId int NOT NULL,
	ProductId int NOT NULL,
	Quantity int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.OrderDetails ADD CONSTRAINT
	PK_OrderDetails PRIMARY KEY CLUSTERED 
	(
	OrderDetailId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.OrderDetails ADD CONSTRAINT
	FK_OrderDetails_Orders FOREIGN KEY
	(
	OrderId
	) REFERENCES dbo.Orders
	(
	OrderId
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.OrderDetails ADD CONSTRAINT
	FK_OrderDetails_Products FOREIGN KEY
	(
	ProductId
	) REFERENCES dbo.Products
	(
	ProductId
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
COMMIT

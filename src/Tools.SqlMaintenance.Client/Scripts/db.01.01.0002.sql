/****** Object:  View [dbo].[ProductSales]    Script Date: 09/02/2009 16:46:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ProductSales]
AS
SELECT     dbo.Customers.Name, dbo.Products.Name AS Expr1, dbo.OrderDetails.Quantity, dbo.Orders.Date
FROM         dbo.Customers INNER JOIN
                      dbo.Orders ON dbo.Customers.CustomerId = dbo.Orders.CustomerId INNER JOIN
                      dbo.OrderDetails ON dbo.Orders.OrderId = dbo.OrderDetails.OrderId INNER JOIN
                      dbo.Products ON dbo.OrderDetails.ProductId = dbo.Products.ProductId

GO


